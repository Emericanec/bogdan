<?php
/**
 * новая страница:
 * 1) создаешь функцию в IndexController.php
 * пример ты хочешь создать страницу /index/moe_nasvanie
 * :
 * public function moe_nasvanie(){
 *   $this->render('moe_nasvanie');
 * }
 *
 * 2) создаешь файл views/moe_nasvanie.php - тут будет весь html код страницы
 * 3) если нужно то добавляеш новую ссылку в views/layout.php
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);
header('Content-Type: text/html; charset=utf-8');

session_start();
include_once("core/Router.php");

$router = new Router();
$router->run();
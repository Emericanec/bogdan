<?php
include_once "core/Controller.php";
include_once "models/User.php";

class UserController extends Controller
{

    public function login(){
        $message = "";
        $user = new User();
        if(!empty($_POST['login']) && !empty($_POST['password'])){
            if($user->findUser($_POST['login'], $_POST['password'])){
                $_SESSION['user'] = 1;
                header("Location: /");
                exit();

            }else{
                $message = "Неверный логин или пароль";
            }
        }
        $this->render('login', array(
            'message' => $message
        ));
    }

    public function registration(){
        $message = "";
        $user = new User();
        if(!empty($_POST['login']) && !empty($_POST['password'])){
            if(!$user->findUser($_POST['login'], $_POST['password'])){
                $user->create($_POST['login'], $_POST['password']);
                $message = "Регистрация прошла успешна";

            }else{
                $message = "Такой пользователь уже существует";
            }
        }
        $this->render('registration', array(
            'message' => $message
        ));
    }

    public function logout(){
        unset($_SESSION['user']);
        header("Location: /");
        exit();
    }

}
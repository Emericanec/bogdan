<?php
include_once('core/Controller.php');

class IndexController extends Controller
{

    public function index(){
        $this->render('index');
    }

    public function add(){
        $this->onlyForUsers(); // только для атворизированных пользователей

        $this->render('add');
    }

    public function theory(){
        $this->onlyForUsers();
        $this->render('theory');
    }

    public function gallery(){
        $this->onlyForUsers();
        $this->render('practice');
    }

    public function video(){
        $this->onlyForUsers();
        $this->render('video');
    }

    public function map(){
        $this->onlyForUsers();
        $this->render('map');
    }
}
<style>
    ul.button_ul li {
        margin-bottom: 10px;
    }
</style>
<div class="row">
    <div class="col-xs-6">
        <img src="/imeg/b6mxTd4isAg.jpg" style="width:500px">
    </div>
    <div class="col-xs-6">
        <h3>Рады приветствовать вас на нашем сайте.</h3>
        Мы приложим все усилия научить вас рисовать. Так же, приветствуется
        выстовка работ наших посетителей, если вам есть что показать, будем рады опубликовать ваши работы, независимо от
        такого на сколько богат ваш опыт.

        Если вы выбрали то, что вам нужно, тогда прошу вас к нам на регистрацию, после чего мы и покажем, как нужно
        рисовать, как будете рисовать вы, если найдете вдохновение.
        <br><br>
        <b>Рассмотрим все виды рисования, какие только существуют в нашем красочном мире.
            И так, выбирай:</b>
        <ul class="button_ul" style="list-style-type: none;">
            <li><a href="/index/theory#a1" class="btn btn-default">Акварель</a></li>
            <li><a href="/index/theory#a2" class="btn btn-default">Гуашь</a></li>
            <li><a href="/index/theory#a3" class="btn btn-default">Алла прима</a></li>
            <li><a href="/index/theory#a4" class="btn btn-default">Горячая эмаль</a></li>
            <li><a href="/index/theory#a5" class="btn btn-default">Граттаж</a></li>
            <li><a href="/index/theory#a6" class="btn btn-default">Гризайль</a></li>
            <li><a href="/index/theory#a7" class="btn btn-default">Декупаж</a></li>
            <li><a href="/index/theory#a8" class="btn btn-default">Монотопия</a></li>
            <li><a href="/index/theory#a9" class="btn btn-default">Монументальная живопись</a></li>
            <li><a href="/index/theory#a10" class="btn btn-default">Пастель</a></li>
            <li><a href="/index/theory#a11" class="btn btn-default">Аэрография</a></li>
        </ul>
    </div>
</div>
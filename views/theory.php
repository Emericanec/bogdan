<div class="row">
    <div class="col-xs-12">
        <h1>Теория</h1>
        <img src="/imeg/L5jqXrzsND0.jpg" style="width:500px">
        <div></div>
        <h3>Рассмотрим следующие термины:</h3>

        <blockquote id="a2">
            <b>Гуашь</b> — материал, состоящий из неорганических красителей с клеевым раствором и добавлением различных
            видов белил. Также в стандартный состав входят пластификаторы, чаще всего это глицерин, антисептики,
            поверхностно-активные вещества, растворимая камедь и декстрин. Для придания цветового насыщения и плотности
            добавляют каолин и искусственно осажденный сульфат бария, так как гуашь склонна к осветлению, после
            высыхания
            нанесенного рисунка. Гуашь легко восстанавливать путем разведения водой (порой туда добавляют
            желатиновый клей или глицерин), после нанесения она быстро сохнет, не имеет сильного резкого запаха. В
            отличие
            от акварели, с ней необязательно соблюдать баланса перехода цветов от светлого к более темному.
            Предварительно
            можно создавать эскиз карандашом и рисовать сразу же на нем. Некоторыми видами гуаши возможно нанесение на
            различные материалы: бумага, ткань, грунтованный холст, фанера, глина и прочие. Она матовая, непрозрачная,
            при
            высыхании приобретает более светлый оттенок.
        </blockquote>

        <blockquote id="a3">
            <b>Алла прима</b> — Техника акварели достаточно трудна и
            разнообразна. Краски разводятся водой, что придает им подвижность, позволяя применять разные приемы: делать
            широкие заливки, вливать один мазок в другой, тонко прорабатывать детали.Работают акварелью по наклонной
            поверхности. На горизонтальной поверхности краска может застаиваться, образуя разводы. Но наклон поверхности
            листа не должен быть близким к вертикальному.При слишком крутом наклоне краска будет быстро стекать вниз и
            создавать опасность подтеков. В акварельной живописи используется подставка, позволяющая регулировать наклон
            поверхности листа.
        </blockquote>

        <blockquote id="a1">
            <b>Акварель</b> — это техника, требующая точности. Исправления в ней трудны, порой невозможны.
                Например, в прописанном этюде уже нельзя существенно исправить рисунок. Сложно бывает исправить и неверно
                положенный цвет. При работе акварелью нужно учитывать уменьшение насыщенности красок после высыхания
                примерно на
                треть. Поэтому цвет в акварели берется с запасом интенсивности и плотности.
        </blockquote>

        <blockquote id="a11">
            <b>Аэрография</b> — прежде
            всего, вам следует приобрести инструменты, необходимые каждому аэрографу. Самый важный инструмент –
            аэрограф.
            Купить его можно в художественном салоне. Принцип работы аэрографа любой модели схож. Отличие только в
            наборе
            комплектующих и цене, которая во многом зависит от производителя. Надо сказать, что аэрограф, изготовленный
            в
            Тайване, работает, как правило, не хуже фирменного варианта.
        </blockquote>

        <blockquote id="a4">
            <b>Горячая эмаль</b> — порошковую эмаль
            разводят водой до сметанообразной консистенции. После нанесения эмаль необходимо полностью высушить, прежде
            чем
            начать обжиг. Иначе лопающиеся пузырьки воздуха испортят эмалевую гладкость покрытия.Дальше изделие
            нагревают в
            специальной муфельной печи. Температура наплавления эмалей на металл — t 790+20. Если же горячая эмаль в
            украшении многоцветная и перегородчатая, то технология работы предусматривает строгую очерёдность обжига
            (плавления эмалевого порошка). Первой наносят жаростойкую эмаль, затем изделие помещается в печь, где эмаль
            затвердевает. Эмаль наносят снова, только берут уже менее жаростойкую, и вновь изделие возвращают в печь, но
            температуру немного снижают. И так далее, главное не испортить украшение. Самая тугоплавкая эмаль – белая,
            за
            ней идут розовая, синяя, зеленая, черная, и в последнюю очередь наносится красная эмаль. Эмаль плавится,
            ровно
            заливая необходимые участки.
        </blockquote>

        <blockquote id="a5">
            <b>Граттаж</b> — это способ выполнения рисунка путем процарапывания острым инструментом бумаги или картона,
            залитых тушью или черной гуашью.Другое название техники — воскография, иногда ее также называют царапкой.
            Рисунки, выполненные в технике граттаж, отличаются контрастом белых линий и черного фона, и похожи на
            гравюры. А
            если предварительно покрасить лист бумаги в различные цвета, то рисунок получится очень интересным и
            оригинальным.
        </blockquote>

        <blockquote id="a6">
            <b>Гризайль</b> — в истории европейской станковой и монументальной живописи есть приемы,
            когда художники выполняли тематические фигуративные и орнаментальные композиции в технике гризайль, при этом
            изображаемые на плоскости объемные предметы решались с использованием градаций тонов от черного до белого.
            Гризайль- от латинского слова "гриз", что в переводе значит серый, и серые тона в этой технике
            преобладающие, но
            при этом они могут иметь большую шкалу градаций. Исследователь техники живописи старых мастеров немец Эрнст
            Бергер в книге "История развития техники масляной живописи" называет гризайль изображением скульптурным
            объемным.
        </blockquote>

        <blockquote id="a7">
            <b>Декупаж</b> — технология декупаж представляет собой процесс украшения различных предметов
            аппликациями (обычно вырезанными из специальной декупажной бумаги, столовых салфеток с красивым
            рисунком, газет,
            книг, журналов и т.д.) и покрытие этих аппликаций несколькими слоями лака. Дизайнерским предметом
            декупажа может
            стать что угодно: мебель, посуда, зеркала, новогодние украшения, тетради, свечи, рамки для фотографий,
            различные
            шкатулки и многое-многое другое. При правильном соблюдении техники декупажа декорируемый предмет
            преображается в
            произведение искусства, а аппликации становятся похожими на декоративную роспись. Создается полная
            видимость
            того, что картинка именно нарисована на предмете. Иногда к обычной лакировке аппликаций также
            добавляются
            специальные эффекты: золочение (патинирование), кракелюр (состаривание, декоративные трещинки на
            предмете),
            наложение теней и др.
        </blockquote>

        <blockquote id="a8">
            <b>Монотипия</b> — простая, но удивительная техника рисования красками (акварелью,
            гуашью и пр.). Она заключается в том, что рисунок рисуется на одной стороне поверхности (это может быть
            не
            только бумажный лист - но об этом в следующих уроках) - и отпечатывается на другую. Подобно как
            отпечаткам - в
            уроке рисования №3, только сложнее - и от этого интереснее! Ведь тут отпечатывается целый рисунок - и
            этот
            момент происходит еще более красивее и завораживающе!
        </blockquote>

        <blockquote id="a9">
            <b>Монументальная живописи</b> — появление живописи
            было неразрывно связано с архитектурой – рисунки древних людей археологи находили на стенах пещер, а
            позже – на
            фасадах зданий – храмов, дворцов, культовых сооружений. Древняя живопись не могла существовать без стен,
            потолков и других сооружений. Художники в те времена еще не были знакомы с искусством рисования на
            холсте.Понятие «монументальная» живопись объединяет техники, которые развивались одновременно с
            искусством
            возведения зданий. Фреска, мозаика, витражи, секко – эти виды живописи украшали здания в течение
            нескольких
            эпох, и даже сегодня интерес к этим техникам по-прежнему актуален.Понятие «монументальная» имеет в
            основе
            латинское слово «монумент», что значит «напоминающий», «хранящий память». Действительно, техники
            монументальной
            живописи отличаются долговечностью. Благодаря им люди много узнали о культуре древних времен, и
            событиях,
            произошедших до нашей эры.
        </blockquote>

        <blockquote id="a10">
            <b>Пастель</b> — техника работы сухой пастелью кажется начинающим не очень
            сложной. Но на деле она требует опыта и последовательности в работе. Пастель позволяет пользоваться
            разнообразными приёмами работы. В то же время нет готовых универсальных рецептов, как нужно работать
            пастелью.
            Пастель допускает большое разнообразие в технике. Каждый художник, работая пастелью, вырабатывает
            собственные
            приёмы работы, которые соответствуют его творческим задачам и темпераменту.
        </blockquote>

    </div>
</div>
<?
include_once "models/User.php";
?>
<html>
<head>
    <meta charset="utf-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
<header class="bs-docs-nav navbar navbar-static-top" id="top" style="border-bottom: 1px solid #ccc;">
    <div class="container " >
        <div class="navbar-header">
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle"
                    data-target="#bs-navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
            <a href="../" class="navbar-brand">Юный художник</a></div>
        <nav class="collapse navbar-collapse" id="bs-navbar">
            <ul class="nav navbar-nav">
                <?if(!User::isGuest()){?>
                <li class=""><a href="/index/theory">Теория</a></li>
                <li class=""><a href="/index/gallery">Галерея</a></li>
                <li class=""><a href="/index/video">Видео</a></li>
                <li class=""><a href="/index/map">Карта</a></li>
                <?}?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?if(User::isGuest()){?>
                <li><a href="/user/login">Вход</a></li>
                <li><a href="/user/registration">Регистрация</a></li>
                <?}else{?>
                    <li><a href="/user/logout">Выход</a></li>
                <?}?>
            </ul>
        </nav>
    </div>
</header>
<div class="container">
    <?= $content ?>
</div>
</body>
</html>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <form method="post">
            <?=$message?>
            <div class="form-group">
                <label>Логин</label>
                <input type="text" name="login" class="form-control" placeholder="Логин">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Пароль</label>
                <input type="password" name="password" class="form-control" placeholder="Пароль">
            </div>
            <button type="submit" class="btn btn-default">Войти</button>
        </form>
    </div>
</div>

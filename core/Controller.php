<?php
include_once "models/User.php";

class Controller
{
    public $layout = 'layout';

    public function render($template, $data = array()){
        $content = $this->renderFile($template, $data);
        $layout = $this->renderFile($this->layout, array(
            'content' => $content,
        ));
        echo $layout;
    }

    public function renderFile($file, $data = array()){
        if (is_array($data) && !empty($data)) {
            extract($data);
        }
        ob_start();
        include "views/{$file}.php";
        return ob_get_clean();
    }

    public function onlyForUsers(){
        if(User::isGuest()){
            header("Location: /");
            exit();
        }
    }
}
<?php


class Router
{

    public $controller = 'index';
    public $action = 'index';

    public function run(){
        $path = $_SERVER['REQUEST_URI'];
        $explode = explode('/', $path);
        if(isset($explode[1])){
            if($explode[1] == ""){
                $explode[1] = 'index';
            }
            $pathToController = "controllers/".$explode[1]."Controller.php";
            if(file_exists($pathToController)){
                $this->controller = $explode[1]."Controller.php";
                include($pathToController);
                $controllerName = $explode[1]."Controller";
                $controller = new $controllerName();
                if(isset($explode[2]) && $explode[2] != ""){
                    if(method_exists($controller, $explode[2])){
                        $this->action = $explode[2];
                    }
                }
                $controller->{$this->action}();
            }
        }
    }

}